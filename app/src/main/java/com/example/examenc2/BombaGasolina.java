package com.example.examenc2;

import java.io.Serializable;
import java.util.ArrayList;

public class BombaGasolina implements Serializable {
    private int id;
    private int numBomba;
    private int capacidadBomba;
    private int tipoGasolina;
    private float precioGasolina;
    private int acumuladorLitrosBomba;

    public BombaGasolina() {

    }

    public BombaGasolina(int numBomba, int capacidadBomba, int tipoGasolina, float precioGasolina, int acumuladorLitrosBomba){
        this.numBomba = numBomba;
        this.capacidadBomba = capacidadBomba;
        this.tipoGasolina =tipoGasolina;
        this.precioGasolina =precioGasolina;
        this.acumuladorLitrosBomba =acumuladorLitrosBomba;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public int getCapacidadBomba() {
        return capacidadBomba;
    }

    public void setCapacidadBomba(int capacidadBomba) {
        this.capacidadBomba = capacidadBomba;
    }

    public int getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(int tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public float getPrecioGasolina() {
        return precioGasolina;
    }

    public void setPrecioGasolina(float precioGasolina) {
        this.precioGasolina = precioGasolina;
    }

    public int getAcumuladorLitrosBomba() {
        return acumuladorLitrosBomba;
    }

    public void setAcumuladorLitrosBomba(int acumuladorLitrosBomba) {
        this.acumuladorLitrosBomba = acumuladorLitrosBomba;
    }
    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }


}