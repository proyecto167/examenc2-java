package com.example.examenc2;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterVenta extends BaseAdapter {
    private List<Venta> datos;
    private Context context;

    public AdapterVenta(Context context, List<Venta> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int position) {
        return datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            // Inflar el diseño personalizado del elemento de la lista
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.ventas_item, parent, false);

            // Crear un ViewHolder para mantener las referencias a las vistas
            viewHolder = new ViewHolder();
            viewHolder.lblCantidad = convertView.findViewById(R.id.lblCantidad);
            viewHolder.lblPrecio = convertView.findViewById(R.id.lblPrecio);
            viewHolder.lblTotal = convertView.findViewById(R.id.lblTotal);

            // Establecer el ViewHolder como una etiqueta de la vista convertida
            convertView.setTag(viewHolder);
        } else {
            // Si convertView no es nulo, obtener el ViewHolder de la etiqueta
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Obtener el elemento de datos para la posición actual
        Venta venta = datos.get(position);

        // Establecer los datos en las vistas del diseño personalizado
        viewHolder.lblCantidad.setText(String.valueOf(venta.getTextCantidadGasolina()));
        viewHolder.lblPrecio.setText(String.valueOf(venta.getTextPrecioGasolina()));

        // Obtener los valores de cantidad y precio
        int cantidad = venta.getTextCantidadGasolina();
        float precio = venta.getTextPrecioGasolina();

        float total = cantidad * precio;

        String totalString = String.format("%.2f", total);
        viewHolder.lblTotal.setText(totalString);

        return convertView;
    }

    private static class ViewHolder {
        TextView lblCantidad;
        TextView lblPrecio;
        TextView lblTotal;
    }
}