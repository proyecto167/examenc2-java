package com.example.examenc2;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import Modelo.VentasDb;

public class MainActivity extends AppCompatActivity {

    private EditText txtNumBomba, txtPrecio, txtCapacidad, txtContador, txtCantidad;
    private Button btnIniciarBomba, btnHacerVenta, btnRegistrarVenta, btnConsultarRegistro, btnLimpiar, btnSalir;
    private TextView lblCantidad, lblPrecio, lblTotalAPagar;
    private RadioGroup radioGroup;
    private RadioButton radioRegular, radioExtra;
    private BombaGasolina bombaGasolina;
    private boolean ventaCreada;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializar los elementos de la interfaz
        txtNumBomba = findViewById(R.id.txtNumBomba);
        txtPrecio = findViewById(R.id.txtPrecio);
        txtCapacidad = findViewById(R.id.txtCapacidad);
        txtContador = findViewById(R.id.txtContador);
        txtCantidad = findViewById(R.id.txtCantidad);
        btnIniciarBomba = findViewById(R.id.btnIniciarBomba);
        btnHacerVenta = findViewById(R.id.btnHacerVenta);
        btnRegistrarVenta = findViewById(R.id.btnRegistrarVenta);
        btnConsultarRegistro = findViewById(R.id.btnConsultarRegistro);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
        lblCantidad = findViewById(R.id.lblCantidadResultado);
        lblPrecio = findViewById(R.id.lblPrecioResultado);
        lblTotalAPagar = findViewById(R.id.lblTotalAPagarResultado);
        radioGroup = findViewById(R.id.radioGroup);
        radioRegular = findViewById(R.id.radioRegular);
        radioExtra = findViewById(R.id.radioExtra);

        radioRegular.setChecked(true);

        // Agregar listeners de eventos a los botones
        btnIniciarBomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numBomba = txtNumBomba.getText().toString().trim();
                String precio = txtPrecio.getText().toString().trim();
                //String capacidad = txtCapacidad.getText().toString().trim();
                //String contador = txtContador.getText().toString().trim();

                String capacidad = "200";
                String contador = "0";

                if (numBomba.isEmpty() || precio.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Llene todos los campos", Toast.LENGTH_SHORT).show();
                    return;
                }

                int numBombaInt;
                int capacidadInt;
                float precioFloat;
                int contadorInt;

                try {
                    numBombaInt = Integer.parseInt(numBomba);
                    capacidadInt = Integer.parseInt(capacidad);
                    precioFloat = Float.parseFloat(precio);
                    contadorInt = Integer.parseInt(contador);
                } catch (NumberFormatException e) {
                    Toast.makeText(MainActivity.this, "Error en el formato de los valores numéricos", Toast.LENGTH_SHORT).show();
                    return;
                }

                int tipoGasolina;
                if (radioRegular.isChecked()) {
                    tipoGasolina = 1; // Gasolina regular
                } else if (radioExtra.isChecked()) {
                    tipoGasolina = 2; // Gasolina extra
                } else {
                    tipoGasolina = 1;
                }

                bombaGasolina = new BombaGasolina(numBombaInt, capacidadInt, tipoGasolina, precioFloat, contadorInt);

                txtCapacidad.setText(capacidad);
                txtContador.setText(contador);

                Toast.makeText(MainActivity.this, "Bomba creada", Toast.LENGTH_SHORT).show();
            }

        });

        btnHacerVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cantidad = txtCantidad.getText().toString().trim();
                if (cantidad.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese una cantidad", Toast.LENGTH_SHORT).show();
                } else if (bombaGasolina != null) {
                    int cantidadInt = Integer.parseInt(cantidad);

                    int capacidad = bombaGasolina.getCapacidadBomba();
                    int contador = bombaGasolina.getAcumuladorLitrosBomba();

                    if (cantidadInt + contador > capacidad) {
                        Toast.makeText(MainActivity.this, "La cantidad supera la capacidad de la bomba de gasolina", Toast.LENGTH_SHORT).show();
                    } else {
                        float precio = bombaGasolina.getPrecioGasolina();
                        float totalVenta = cantidadInt * precio;

                        lblCantidad.setText(cantidad);
                        lblPrecio.setText(String.valueOf(precio));
                        lblTotalAPagar.setText(String.valueOf(totalVenta));
                        ventaCreada = true;

                        // Sumar la cantidad vendida al contador de la bomba de gasolina
                        bombaGasolina.setAcumuladorLitrosBomba(contador + cantidadInt);
                        txtContador.setText(String.valueOf(contador + cantidadInt));
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Debe iniciar la bomba de gasolina primero", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnRegistrarVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (bombaGasolina != null) {
                String cantidad = lblCantidad.getText().toString().trim();

                if (cantidad.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingrese una cantidad", Toast.LENGTH_SHORT).show();
                    return;
                }

                int numBombaDb = bombaGasolina.getNumBomba();
                int tipoGasolinaDb = bombaGasolina.getTipoGasolina();
                float precioGasolinaDb = bombaGasolina.getPrecioGasolina();
                int cantidadGasolinaBombaDb = Integer.parseInt(cantidad);


                if (ventaCreada) {
                    // Crear una instancia de Venta con los valores obtenidos
                    Venta venta = new Venta(numBombaDb, tipoGasolinaDb, precioGasolinaDb, cantidadGasolinaBombaDb);

                    // Crear una instancia de VentasDb
                    VentasDb ventasDb = new VentasDb(MainActivity.this);

                    // Insertar la venta en la base de datos
                    long ventaId = ventasDb.insertVenta(venta);

                    if (ventaId != -1) {
                        Toast.makeText(MainActivity.this, "Venta registrada correctamente", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Error al registrar la venta", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "No se ha realizado una venta", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MainActivity.this, "Bomba no iniciada", Toast.LENGTH_SHORT).show();
            }
        }
    });

        btnConsultarRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                startActivity(intent);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //txtNumBomba.setText("");
                //txtPrecio.setText("");
                //txtCapacidad.setText("");
                //txtContador.setText("");
                txtCantidad.setText("");

                lblCantidad.setText("0");
                lblPrecio.setText("0");
                lblTotalAPagar.setText("0");
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Confirmación");
                builder.setMessage("¿Está seguro de que desea cerrar el programa?");

                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }
}