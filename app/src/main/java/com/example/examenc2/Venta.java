package com.example.examenc2;

import java.io.Serializable;
import java.util.ArrayList;

public class Venta implements Serializable {
    private int id;
    private int numBomba;
    private int tipoGasolina;
    private float precioGasolina;
    private int cantidadGasolina;

    public Venta() {

    }

    public Venta(int numBomba, int tipoGasolina, float precioGasolina, int cantidadGasolina){
        this.numBomba = numBomba;
        this.tipoGasolina = tipoGasolina;
        this.precioGasolina =precioGasolina;
        this.cantidadGasolina =cantidadGasolina;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public Integer getTextNumBomba() { return numBomba; }
    public void setTextNumBomba(int numBomba) { this.numBomba = numBomba; }
    public Integer getTextTipoGasolina() { return tipoGasolina; }
    public void setTextTipoGasolina(int tipoGasolina) { this.tipoGasolina = tipoGasolina; }
    public Float getTextPrecioGasolina(){ return precioGasolina; }
    public void setTextPrecioGasolina(float precioGasolina){
        this.precioGasolina = precioGasolina;
    }
    public Integer getTextCantidadGasolina() { return cantidadGasolina; }
    public void setTextCantidadGasolina(int cantidadGasolina) { this.cantidadGasolina = cantidadGasolina; }

    public static ArrayList<Venta> llenarVentas(){
        ArrayList<Venta> ventas=new ArrayList<>();

        ventas.add(new Venta(11,1,12.00f,10));

        return ventas;
    }

}

