package com.example.examenc2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Modelo.VentasDb;

public class ListaActivity extends AppCompatActivity {

    private ListView listView;
    private AdapterVenta adapter;
    private List<Venta> listaVentas;
    private VentasDb ventasDb;
    private Button btnRegresar;
    private TextView lblTotalVentaRes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_activity);

        ventasDb = new VentasDb(this);

        // Obtener la referencia de la ListView
        listView = findViewById(R.id.lstRegistros);

        btnRegresar = findViewById(R.id.btnRegresar);

        // Crear la lista de ventas
        ArrayList<Venta> ventas = ventasDb.allVentas();
        adapter = new AdapterVenta(this, ventas);
        listView.setAdapter(adapter);

        lblTotalVentaRes = findViewById(R.id.lblTotalVentaRes);

        calcularTotal();

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Regresar a la pantalla anterior
                Intent intent = new Intent(ListaActivity.this, MainActivity.class);
                startActivity(intent);
                finish(); // Cierra la actividad actual (MainActivity)
            }
        });

    }

    private void calcularTotal() {
        float total = 0;
        listaVentas = ventasDb.allVentas();

        for (Venta venta : listaVentas) {
            total += venta.getTextCantidadGasolina() * venta.getTextPrecioGasolina();
        }
        lblTotalVentaRes.setText(String.valueOf(total));
    }

}