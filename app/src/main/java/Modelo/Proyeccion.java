package Modelo;

import android.database.Cursor;

import com.example.examenc2.Venta;

import java.util.ArrayList;

public interface Proyeccion {

    public Venta getVenta(String id);
    public ArrayList<Venta> allVentas();
    public Venta readVenta(Cursor cursor);

}
