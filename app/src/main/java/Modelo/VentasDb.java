package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.examenc2.Venta;

import java.util.ArrayList;

public class VentasDb implements Persistencia, Proyeccion{

    private Context context;
    private VentasDbHelper helper;
    private SQLiteDatabase db;

    public VentasDb(Context context, VentasDbHelper helper){
        this.context=context;
        this.helper=helper;
    }

    public VentasDb(Context context){
        this.context=context;
        this.helper=new VentasDbHelper(this.context);
    }


    @Override
    public void openDataBase() {
        db=helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    public long insertVenta(Venta venta) {

        ContentValues values=new ContentValues();

        values.put(DefineTable.Ventas.COLUMN_NAME_NUMBOMBA, venta.getTextNumBomba());
        values.put(DefineTable.Ventas.COLUMN_NAME_TIPOGASOLINA, venta.getTextTipoGasolina());
        values.put(DefineTable.Ventas.COLUMN_NAME_PRECIOGASOLINA, venta.getTextPrecioGasolina());
        values.put(DefineTable.Ventas.COLUMN_NAME_CANTIDADGASOLINA, venta.getTextCantidadGasolina());

        this.openDataBase();
        long num = db.insert(DefineTable.Ventas.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar","insertVenta: "+num);

        return num;
    }

    @Override
    public long updateVenta(Venta venta) {
        ContentValues values=new ContentValues();

        values.put(DefineTable.Ventas.COLUMN_NAME_NUMBOMBA, venta.getTextNumBomba());
        values.put(DefineTable.Ventas.COLUMN_NAME_TIPOGASOLINA, venta.getTextTipoGasolina());
        values.put(DefineTable.Ventas.COLUMN_NAME_PRECIOGASOLINA, venta.getTextPrecioGasolina());
        values.put(DefineTable.Ventas.COLUMN_NAME_CANTIDADGASOLINA, venta.getTextCantidadGasolina());

        this.openDataBase();
        long num = db.update(
                DefineTable.Ventas.TABLE_NAME,
                values,
                DefineTable.Ventas.COLUMN_NAME_ID+"="+venta.getId(),
                null);
        this.closeDataBase();
        Log.d("agregar","insertVenta: "+num);

        return num;
    }

    @Override
    public void deleteVentas(int id) {
        this.openDataBase();
        db.delete(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.COLUMN_NAME_ID+"=?",
                new String[] {String.valueOf(id)});
        this.closeDataBase();
    }


    @Override
    public Venta getVenta(String id) {
        db = helper.getWritableDatabase();

        String stringId = String.valueOf(id);

        Cursor cursor = db.query(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.REGISTRO,
                DefineTable.Ventas.COLUMN_NAME_ID + " = ?",
                new String[]{stringId},
                null, null, null);

        if (cursor.moveToFirst()) {
            Venta venta = readVenta(cursor);
            cursor.close();
            return venta;
        } else {
            cursor.close();
            return null; // No se encontró en la base de datos
        }
    }

    @Override
    public ArrayList<Venta> allVentas() {
        db=helper.getWritableDatabase();

        Cursor cursor=db.query(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.REGISTRO,
                null, null, null, null, null);
        ArrayList<Venta> ventas = new ArrayList<Venta>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            Venta venta=readVenta(cursor);
            ventas.add(venta);
            cursor.moveToNext();
        }

        cursor.close();
        return ventas;
    }

    @Override
    public Venta readVenta(Cursor cursor) {
        Venta venta=new Venta();

        venta.setId(cursor.getInt(0));
        venta.setTextNumBomba(cursor.getInt(1));
        venta.setTextTipoGasolina(cursor.getInt(2));
        venta.setTextPrecioGasolina(cursor.getFloat(3));
        venta.setTextCantidadGasolina(cursor.getInt(4));

        return venta;
    }
}
