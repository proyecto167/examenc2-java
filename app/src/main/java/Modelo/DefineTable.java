package Modelo;

public class DefineTable {

    public DefineTable(){}

    public static abstract class Ventas{
        public static final String TABLE_NAME="ventas";
        public static final String COLUMN_NAME_ID="id";
        public static final String COLUMN_NAME_NUMBOMBA="numBomba";
        public static final String COLUMN_NAME_TIPOGASOLINA="tipoGasolina";
        public static final String COLUMN_NAME_PRECIOGASOLINA="precioGasolina";
        public static final String COLUMN_NAME_CANTIDADGASOLINA="cantidadGasolina";

        public static String[] REGISTRO = new String[]{
                Ventas.COLUMN_NAME_ID,
                Ventas.COLUMN_NAME_NUMBOMBA,
                Ventas.COLUMN_NAME_TIPOGASOLINA,
                Ventas.COLUMN_NAME_PRECIOGASOLINA,
                Ventas.COLUMN_NAME_CANTIDADGASOLINA
        };
    }
}
