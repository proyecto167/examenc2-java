package Modelo;

import com.example.examenc2.Venta;

public interface Persistencia {

    public void openDataBase();
    public void closeDataBase();
    public long insertVenta(Venta venta);
    public long updateVenta(Venta venta);
    public void deleteVentas(int id);

}
